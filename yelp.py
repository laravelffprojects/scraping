from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import re
import requests
from selenium import webdriver
import parameters
from parsel import Selector
import csv
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
url = r"https://www.yelp.com/search?find_desc=Retail%20Stores&find_loc=Van%20Nuys%2C%20Los%20Angeles%2C%20CA&ns=1"
driver = webdriver.Chrome(r"C:\Users\STORM\chromedriver.exe")
driver.set_window_position(0, 0)
driver.set_window_size(1920, 1068)
driver.get(url)


def getsoup(url):
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    page = urlopen(req)
    html_bytes = page.read()
    html = html_bytes.decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")
    return soup


soup = getsoup(url)
rows = soup.find('ul', {'class': ['undefined list__09f24__17TsU']})

websites = []
information = {}
for index, row in enumerate(rows):
    if index < 5:
        continue
    if index >= 15:
        break
    hasBlock = row.find('div', {'class': ['container__09f24__21w3G hoverable__09f24__2nTf3 margin-t3__09f24__5bM2Z margin-b3__09f24__1DQ9x padding-t3__09f24__-R_5x padding-r3__09f24__1pBFG padding-b3__09f24__1vW6j padding-l3__09f24__1yCJf border--top__09f24__1H_WE border--right__09f24__28idl border--bottom__09f24__2FjZW border--left__09f24__33iol border-color--default__09f24__R1nRO']})
    if hasBlock:
        li = row.find('a', {'class': [
            'link__09f24__1kwXV link-color--blue-dark__09f24__2DRa0 link-size--inherit__09f24__2Uj95']})
        information['business_name'] = row.find('a', {'class': [
            'link__09f24__1kwXV link-color--inherit__09f24__3PYlA link-size--inherit__09f24__2Uj95']}).text
        if li:

            driver.get(f"https://www.yelp.com{li['href']}")

            try:
                website_url = driver.find_element(
                    By.XPATH, '//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/section[2]/div/div[1]/div/div[2]/p[2]/a')

                if website_url:
                    information['business_website_url'] = website_url.text
                    driver.get(f'https://www.google.com')
                    time.sleep(1)
                    search_query = driver.find_element_by_name('q')
                    search_query.send_keys(
                        f"{information['business_website_url']} email")
                    time.sleep(0.5)
                    search_query.send_keys(Keys.RETURN)
                business_address = driver.find_element(
                    By.XPATH, '//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/section[5]/div[2]/div[1]/div/div/div/div[1]/address')
                if business_address:
                    information['business_address'] = business_address.text
                phone_number = driver.find_element(
                    By.XPATH, '//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/section[2]/div/div[2]/div/div[2]/p[2]')
                if phone_number:
                    information['phone_number'] = phone_number.text

            except Exception as e:
                pass
        else:

            driver.get(url)
            try:
                phone_number = driver.find_element(
                    By.XPATH, '/html/body/yelp-react-root/div/div[4]/div/div[1]/div[1]/div[2]/div[2]/ul/li[9]/div/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div/div/p')
                if phone_number:
                    information['phone_number'] = phone_number.text
                business_address = driver.find_element(
                    By.XPATH, '/html/body/yelp-react-root/div/div[4]/div/div[1]/div[1]/div[2]/div[2]/ul/li[15]/div/div/div/div[2]/div[1]/div/div[2]/div/address/div/div/div/p/span')
                if business_address:
                    information['business_address'] = business_address.text
            except Exception as e:
                pass
    print(information)
