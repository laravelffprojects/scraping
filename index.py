from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import re
import requests
from selenium import webdriver
import parameters
from parsel import Selector
import csv

import time
# I need someone to scrape all the companies from this page and provide:
# - Company Name
# - Company Website
# - Company LinkedIn Link
# - Number of Employees (from Linkedin)
# - Industry (from Linkedin)

# There are about 425 companies.
url = "https://business.masstlc.org/directory/Find?term=&advFilter=eyJDdXN0b21GaWVsZFZhbHVlcyI6W10sIkNhdGVnb3J5VmFsdWVzIjpbXSwiQ3VzdG9tRmllbGRzIjpbIjU2ODciXX0%3D"
# imports

# defining new variable passing two parameters
writer = csv.writer(open(parameters.file_name, 'w', newline=''))

# writerow() method to the write to the file object
writer.writerow(['Company', 'Company Website',
                 'Company LinkedIn', 'Number of Employees', 'Industry'])

# specifies the path to the chromedriver.exe
driver = webdriver.Chrome(r"C:\Users\STORM\chromedriver.exe")

driver.get('https://www.linkedin.com')

# locate email form by_class_name
username = driver.find_element_by_id('session_key')

# send_keys() to simulate key strokes
username.send_keys(parameters.linkedin_username)

# sleep for 0.5 seconds

# locate password form by_class_name
password = driver.find_element_by_id('session_password')

# send_keys() to simulate key strokes
password.send_keys(parameters.linkedin_password)

driver.find_element_by_class_name('sign-in-form__submit-button').click()


def getSoup(url, linked_in=False):

    try:
        if linked_in:
            if 'groups' in url:
                return
            # try:
            #     client = requests.Session()

            #     HOMEPAGE_URL = 'https://www.linkedin.com'
            #     LOGIN_URL = 'https://www.linkedin.com/uas/login-submit'

            #     html = client.get(HOMEPAGE_URL).content
            #     soup = BeautifulSoup(html, "html.parser")
            #     csrf = soup.find('input', dict(name='loginCsrfParam'))['value']

            #     login_information = {
            #         'session_key': 'fortanpireva99@gmail.com',
            #         'session_password': 'weather.com',
            #         'loginCsrfParam': csrf,
            #     }

            #     client.post(LOGIN_URL, data=login_information)
            #     print("Login Successful")
            # except Exception as e:
            #     return ''

            # html = client.get(f'{url}about/').content
            # soup = BeautifulSoup(html, "html.parser")
            # return soup
            # driver.get method() will navigate to a page given by the URL address

            driver.get(f'{url}/about/')
            sel = Selector(text=driver.page_source)
            print(sel.xpath(
                '/html/body/div[7]/div[3]/div/div[3]/div[2]/div[2]/div[1]/div[1]/section/dl/dd[2]/text()').extract_first())
            industry = sel.xpath(
                '/html/body/div[7]/div[3]/div/div[3]/div[2]/div[2]/div[1]/div[1]/section/dl/dd[2]/text()').extract_first()
            if industry:
                industry = industry.strip()
            num_employees = sel.xpath(
                '/html/body/div[7]/div[3]/div/div[3]/div[2]/div[2]/div[1]/div[1]/section/dl/dd[3]/text()').extract_first()
            if num_employees:
                num_employees = num_employees.strip()
            return [industry, num_employees]

        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        page = urlopen(req)
        html_bytes = page.read()
        html = html_bytes.decode("utf-8")
        soup = BeautifulSoup(html, "html.parser")
        return soup
    except Exception as e:
        return e


linked_in_employee_class = "org-about-company-module__company-size-definition-text"
linked_in_dl = "overflow-hidden"
number_in_dl = 5
linked_in_icon = "fa-linkedin"
soup = getSoup(url)
rows = soup.find_all('div', {'class': ['mn-row-inner']})


for row in rows:
    industry = ''
    num_employees = ''
    linked_in_url = ''
    company_title = row.find('a', {'class': ['mn-main-heading']}).text
    inner_div = row.find('div', {'class': ['mn-listing-website']})
    if inner_div == None:
        website_link = ""
    else:
        website_link = inner_div.find_all('a')[0]['href']
        soup1 = getSoup(website_link)
        if soup1 != "":
            try:
                linked_in_url = soup1.find(
                    'a', {'href': re.compile(r'linkedin\.com/')})['href']
                soup2 = getSoup(linked_in_url, True)
                if soup2 != '':
                    industry = soup2[0]
                    num_employees = soup2[1]

            except Exception as e:
                pass

    print(company_title, website_link, linked_in_url, industry, num_employees)

    writer.writerow([company_title, website_link,
                     linked_in_url, industry, num_employees])
    print('-----------------------------------------------------------------')

driver.quit()
